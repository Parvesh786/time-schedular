<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function GetWeekDayArray()
    {
        return [
            [
                'day' => 1,
                'name' => 'Sunday',
                'slug' => 'sunday',
            ],
            [
                'day' => 2,
                'name' => 'Monday',
                'slug' => 'monday',
            ],
            [
                'day' => 3,
                'name' => 'Tuesday',
                'slug' => 'tuesday',
            ],
            [
                'day' => 4,
                'name' => 'Wednesday',
                'slug' => 'wednesday',
            ],
            [
                'day' => 5,
                'name' => 'Thursday',
                'slug' => 'thursday',
            ],
            [
                'day' => 6,
                'name' => 'Friday',
                'slug' => 'friday',
            ],
            [
                'day' => 7,
                'name' => 'Saturday',
                'slug' => 'saturday',
            ],
        ];
    }


    public function GetUserScheduleEvents($user_id)
    {
        $schedule_events = \App\Models\Schedules::where('user_id',$user_id)->with(['Event'])->get();   
        $events = [];
        foreach ($schedule_events as $key => $value) 
        {
            $event_color_coding = 'fc-event-white fc-event-solid-primary fc-event-gradient';

            if($value->Event->priority=='high'){
                $event_color_coding = 'fc-event-white fc-event-solid-success';
            }else if($value->Event->priority=='moderate'){
                $event_color_coding = 'fc-event-white fc-event-solid-primary';
            }


            $events[] = [
                'title' =>  $value->Event->event_name,
                'start' => $value->schedule_date.'T'.$value->Event->start_time,
                'end' =>  $value->schedule_date.'T'.$value->Event->end_time,
                'className' =>  $event_color_coding
            ];
        }
        return $events;
    }

}
