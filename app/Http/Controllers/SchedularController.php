<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;

class SchedularController extends Controller
{
    /**
     * Get Scheduled Events Array
     * @return response
     * 
     */
    public function GetScheduledEvents(Request $request)
    {
        $Auth = \Auth::user();
        $events =  $this->GetUserScheduleEvents($Auth->id);

        return response()->json([
            'status' => true,
            'data' => $events
        ],200);
    }

    /**
     * Create Event Method
     * @return response
     */
    public function CreateEvent(Request $request)
    {
        $request->validate([
            'event_name' => 'required|max:191',
            'start_time' => 'required',
            'end_time' => 'required',
            'week_days' => 'required|array',
            'event_priority' => 'nullable|in:high,moderate,low',
        ]);

        $selected_week_days = $request->input('week_days');
        $date_based_on_week = null;
        $selected_dates_based_on_week = [];
        
        // Step 1 Loop Out Through the Selected Week Days
        $end_date = Carbon::now()->addDay(90);
        
        foreach ($selected_week_days as $week_key => $week_day) {
            $date_based_on_week = now()->parse($week_day);
            // Step 2:- Get The Reoccuring Dates For elected day of Week
            $selected_dates_based_on_week[] = $this->CalculateScheduleEvent($end_date,$date_based_on_week);
        }

        $Auth = \Auth::user();

        // Step 3:- Create Event
        $event = \App\Models\Events::create([
            'user_id' => $Auth->id,
            'event_name' => htmlentities($request->input('event_name')),
            'event_description' => htmlentities($request->input('event_description')),
            'start_time' => htmlentities($request->input('start_time')),
            'end_time' => htmlentities($request->input('end_time')),
            'priority' => htmlentities($request->input('event_priority')),
            'created_at' => now(),
            'updated_at' => now()
        ]);

        $schedule_array = [];
        foreach ($selected_dates_based_on_week as $key => $value) 
        {
            foreach ($value as $key => $dates) {
                $schedule_array[] = [
                    'user_id' => $Auth->id,
                    'event_id' => $event->event_id,
                    'schedule_date' => date('Y-m-d',strtotime($dates)),
                    'created_at' => now(),
                    'updated_at' => now()
                ];
            }
        }

        // Step 4:- Create Reoccuring Event
        \App\Models\Schedules::insert($schedule_array);

        return response()->json([
            'status' => true,
            'message' => 'Event Created Successfully',
        ],200);
    }

    /**
     * Function TO Calcute Schedule Events Between Two Dates 
     * with a Gap of Week
     *  @param end_bound // Last day upto which we want to calculate 
     * @param initla_date //Start Date
     * @return array
     */
    public function CalculateScheduleEvent($end_bound,$initial_date)
    {
        $_start_date = Carbon::parse($initial_date);
        $dates_array = [];
        while ($_start_date <= $end_bound) 
        {
            $dates_array[] = $_start_date->format('Y-m-d');
            $_start_date->addWeeks(1);
        }

        return $dates_array;
    }
}
