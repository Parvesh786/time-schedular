<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Events extends Model
{
    use HasFactory;

    protected $primaryKey="event_id";
    
    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable=[
        'event_id',
        'user_id',
        'event_name',
        'event_description',
        'start_time',
        'end_time','priority',
        'status',
        'created_at','updated_at'
    ];

}
