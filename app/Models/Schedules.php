<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Schedules extends Model
{
    use HasFactory;

    protected $primaryKey="schedule_id";

    protected $fillable = [
        'user_id',
        'event_id',
        'schedule_date',
        'status',
        'created_at','updated_at'
    ];

    /**
     * Get the Event that owns the Schedules
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Event()
    {
        return $this->belongsTo(\App\Models\Events::class, 'event_id', 'event_id');
    }

}
