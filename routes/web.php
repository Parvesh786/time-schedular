<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});

Auth::routes();


Route::middleware(['auth'])->group(function () 
{
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    
    // Route for Creating Scheduled Event
    Route::post('/create-event',[App\Http\Controllers\SchedularController::class, 'CreateEvent'])->name('schedule.event');
    
    // Get Created Events List 
    Route::post('/schedule-events',[App\Http\Controllers\SchedularController::class, 'GetScheduledEvents'])->name('user.get.scheduled.events');
});